package com.provider.service.impl;

import com.common.entity.User;
import com.common.service.UserService;
import com.provider.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author YanQiQi
 * @Date 2020-04-12 21:16
 **/
@Service
public class UserImpl implements UserService {

    private final UserMapper userMapper;

    @Autowired
    public UserImpl(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    /**
     * 查找用户
     *
     * @return User
     */
    @Override
    public User findUser(Long id) {
        return userMapper.getOne(id);
    }
}
