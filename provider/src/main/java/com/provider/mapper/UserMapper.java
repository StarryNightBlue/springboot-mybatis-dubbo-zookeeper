package com.provider.mapper;

import com.common.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @Author YanQiQi
 * @Date 2020-04-12 21:23
 **/
@Mapper
public interface UserMapper {

    /**
     * 获取所有用户
     *
     * @return List<User>
     */
    @Results(id = "userMap", value = {
        @Result(column = "id", property = "id"),
        @Result(column = "name", property = "name"),
        @Result(column = "age", property = "age"),
        @Result(column = "sex", property = "sex")})
    @Select("SELECT * FROM u_user")
    List<User> getAll();

    /**
     * 根据用户 id 查询单个用户
     *
     * @param id 用户 id
     * @return User
     */
    @Select("SELECT * FROM u_user t WHERE t.id = #{id}")
    @ResultMap("userMap")
    User getOne(Long id);

}
