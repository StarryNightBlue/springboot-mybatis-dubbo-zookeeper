package com.customer.controller;

import com.common.entity.User;
import com.common.service.UserService;
import jdk.nashorn.internal.ir.annotations.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author YanQiQi
 * @Date 2020-04-12 21:38
 **/
@RestController
public class UserController {

    @Resource
    @Reference
    private UserService userService;

    @GetMapping("getUser")
    public User getUser(@RequestParam String id) {
        System.out.println("进入Controller...");
        return userService.findUser(Long.valueOf(id));
    }

}
