package com.common.service;

import com.common.entity.User;

/**
 * @Author YanQiQi
 * @Date 2020-04-12 21:06
 **/
public interface UserService {

    /**
     * 根据 id 查找用户
     *
     * @param id 用户 id
     * @return User
     */
    User findUser(Long id);
}
